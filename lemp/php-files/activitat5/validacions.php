<?php

    $priceErr = "";
    $taxErr = "";
    $valid = true;

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $price = validate($_POST["price"]);
        if (!preg_match("/^-?(?:\d+|\d*\.\d+)$/", $price)) {
            $priceErr = "Please enter a number.";
            $valid = false;   
        }
        $tax = validate($_POST["tax"]);
        if (!preg_match("/^[1-9]$|^[1-9][0-9]$|^(100)$/", $tax)) {
            $taxErr = "Please enter a valid Tax number.";
            $valid = false;
        }   
    }

    function validate($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    global $valid;
?>