<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
      <style>
        .error {color: #FF0000;}
    </style>
   </head>
   <body>
        <?php
            include "validacions.php"
        ?>
        <div class="container mt-3">
            <h2>PRICE, TAX and ROUNDS</h2>
            <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
            <div class="col-auto">
                Price with TAX: 
            </div>
            <div class="row">
                    <div class="col-md-6">
                    <input type="float" name="price"  class="form-control" required value="<?php echo isset($_POST['price']) ? $_POST['price'] : '';?>"/>
                </div>
                <div class="col-auto">
                    <span class="error">
                        * <?php echo $priceErr;?>
                    </span>
                </div>
            </div>
            <div class="col-auto">
                    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
                    TAX (%): 
                </div>
                <div class="row">
                <div class="col-md-6">
                    <input type="integer" name="tax" class="form-control" required value="<?php echo isset($_POST['tax']) ? $_POST['tax'] : '';?> "/>
                </div>
                <div class="col-auto">
                    <span class="error">
                        * <?php echo $taxErr;?>
                    </span>
                </div>
            </div>
            <br>
            <input type="submit" value="CALCULATE" name="execute" class="btn btn-primary"/>
            </form>
       
        <?php
            include "form.php"
        ?>
        </div>
   </body>
</html>
