<?php


if ($_POST) {
    if (isset($_POST['execute'])) {
        execute($valid);
    }
}

function execute($valid)
{
        $price = "";
        $tax = "";

        if (isset($_POST['execute'])) {

            if (isset($_POST['price']) && $_POST['price'] != "") {
                $price = $_POST['price'];
            }
            if (isset($_POST['tax']) && $_POST['tax'] != "") {
                $tax = $_POST['tax'];
            }
        }
        if ($valid) {
            $price_tax = ((int)$price / (1+ (int)$tax/100));

            print "  <h2>TAX data:</h2>\n";
            print "\n";
    
            print "<p>Price without tax: $price_tax €</p>";
            print "<p>Round to 2 decimals using round():". round($price_tax,2)." €</p>";
            print "<p>Using function sprintf():" .sprintf("%01.4f", $price_tax). " €</p>";
            
            echo "  </p>\n";
            echo "\n";
        }
}
?>