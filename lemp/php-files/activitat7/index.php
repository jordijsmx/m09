<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
        <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    </head>
    <body>
    <div class="fondo">
        <div class="fondo2">
            <h2>Exponent Calculator: n!</h2>
            <div class="factorial">     
                <div class="calcul">
                    <?php include "power.php";?>
                    <p>Exponent calculator x<sup>n</sup><p>
                    <div class="gris">
                        <form method="POST">
                            <label for="numero">x =</label>
                            <input class ="base" type="number" step="any" name="base" placeholder="Enter a base" value="<?php echo isset($_POST['base']) ? $_POST['base'] : ''; ?>">
                            <br>
                            <br>
                            <label for="numero">n =</label>
                            <input class ="exponent" type="number" step="any" name="exponent" placeholder="Enter an exponent" value="<?php echo isset($_POST['exponent']) ? $_POST['exponent'] : ''; ?>">
                            <br>
                            <br>
                            <button class="boto" type="submit" name="execute">SEND</button>
                        </form>
                            <p>Exponent calculator:</p>
                            <p>x<sup>n</sup> = <?php echo execute(); ?></p>
                    </div>
                </div>
            </div>

            <br>
            <h5>Positive exponent</h5>
            <p>Exponentiation is a mathematical operation, written as xn, involving the base x and an
                exponent n. In the case where n is a positive integer, exponentiation corresponds to
                repeated multiplication of the base, n times.</p>
            <p> \[x^2 = x · x · ......· x\]</p>
            <p>\[n \space times\]</p>
            <p>The calculator above accepts negative bases, but does not compute imaginary numbers. It
                also does not accept fractions, but can be used to compute fractional exponents, as long
                as the exponents are input in their decimal form</p>
            <br>
            <h5>Negative exponent</h5>
            <p>When an exponent is negative, the negative sign is removed by reciprocating the base and
                raising it to the positive exponent.
            </p>
            <p> \[x^n = {1 \over x · x ......· x}\]</p>
            <p>\[n \space times\]</p>
            <p>So the factorial n! is equal to the number n times the factorial of n minus one. This recurses
                until n is equal to 0</p>
            <br>
            <h5>Exponent 0</h5>
            <p>When an exponent is 0, any number raised to the 0 power is 1.</p>
            <p> \[x^n = 1\]</p>
            <p>
                For 0 raised to the 0 power the answer is 1 however this is considered a definition and not
                an actual calculation
            </p>
            <br>
            <h5>Real number exponent</h5>
            <p>For real numbers, we use the PHP function pow(n,x).</p>

            <br>
        </div>
    </div>
    </body>
</html>