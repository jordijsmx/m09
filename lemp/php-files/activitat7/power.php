<?php
    
    function positiveExponent($base, $exponent) {

        $resultat = 1;
        for ($i = 0; $i < $exponent; $i++) {
            $resultat *= $base;
        }
        return $resultat;
    }

    function negativeExponent($base, $exponent) {

        return 1 / positiveExponent($base, -$exponent);
    }

    function realNumber($base, $exponent) {

        return pow($base, $exponent);
    }

    function execute() {

        if(isset($_POST['execute'])) {
            $base = isset($_POST['base']) ? $_POST['base'] : 0;
            $exponent = isset($_POST['exponent']) ? $_POST['exponent'] : 0;

            if ($exponent == 0) {
                return 1;
            }
            
            if (is_int($exponent)) {

                if ($exponent < 0) {
                    return negativeExponent($base, $exponent);
                } else {
                    return positiveExponent($base, $exponent);
                }
                
            } else {
                return realNumber($base, $exponent);
            }
        }
    }
?>