<?php
        for ($degree = 0; $degree <= 360; $degree++) {
            
            $radian = deg2rad($degree);
            $sin = sin($radian); 
            $cos = cos($radian); 

            $rad_class = $radian < 0 ? 'negative' : 'positive';
            $sin_class = $sin < 0 ? 'negative' : 'positive';
            $cos_class = $cos < 0 ? 'negative' : 'positive';

            echo "<tr>";
            echo "<td>$degree</td>";
            echo "<td class='$rad_class'>" . round($radian, 4) . "</td>";
            echo "<td class='$sin_class'>" . round($sin, 4) . "</td>";
            echo "<td class='$cos_class'>" . round($cos, 4) . "</td>";
            echo "</tr>";
        }
?>