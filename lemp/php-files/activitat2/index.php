<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="UTF-8">
      <link rel="stylesheet" href="style.css">
   </head>
   <body>
        <h1>Sine and Cosine</h1>
        <img src="https://upload.wikimedia.org/wikipedia/commons/7/71/Sine_cosine_one_period.svg">
        <br>
        <table>
            <tr>
                <th>Degrees</th>
                <th>Radians</th>
                <th>Sine</th>
                <th>Cosine</th>
            </tr>
            <?php 
                include 'sincos.php';
            ?>
        </table>
   </body>
</html>