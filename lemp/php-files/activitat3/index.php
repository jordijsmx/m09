<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
   </head>
   <body>
        <div class="container mt-4">
            <table class="table table-primary table-sm table-bordered">
                <?php
                    include 'arrays.php';
                ?>
            </table>

            <table class="table table-success table-sm table-bordered">
                <?php
                    include 'arrays2.php';
                ?>
            </table>
            <br>
                <?php
                    include 'arrays3.php';
                ?>
            <br>
            <table class="table table-success table-sm table-bordered">
                <?php
                    include 'arrays4.php';
                ?>
            </table>

            <table class="table table-success table-sm table-bordered">
                <?php
                    include 'arrays5.php';
                ?>
            </table>

            <table class="table table-success table-sm table-bordered">
                <?php
                    include 'arrays6.php';
                ?>
            </table>
        </div>
   </body>
</html>