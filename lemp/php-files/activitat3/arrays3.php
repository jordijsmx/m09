<?php
   
    global $array;
    $len = 20; 

    $parells = [];
    $imparells = [];

    
    for ($i = 0; $i < $len; $i++) {
        if ($array[$i] % 2 == 0) {
            $parells[] = $array[$i];
        } else {
            $imparells[] = $array[$i];
        }
    }


    echo "Total Evens: ". array_sum($parells) ."<br>";
    echo "Total Odds: ". array_sum($imparells) ."<br>";
    echo "Total Even and Odds: ". array_sum($array) ."<br>";
    echo "Total Average: ". array_sum($array) / $len ."<br>";

?>