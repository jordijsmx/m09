<?php
    
    global $array;
    $len = 20; 


    $menors_mitjana = [];

    echo "<h5>Array with less or equal than average:</h5>";

    $mitjana = array_sum($array) / $len;

    foreach ($array as $valor) {
        if ($valor <= $mitjana) {
            $menors_mitjana[] = $valor;
        }
    }


    for ($i = 0; $i < $len; $i++) {
        echo "<th>$i</th>";
    }
    echo "</tr>";

    echo "<tr>";
    foreach ($menors_mitjana as $valor) {
        echo "<td>$valor</td>";
    }

?>