<?php

function tirarDaus($daus) {

    $resultat = array(); 

    for ($i = 0; $i < $daus; $i++) {
        $resultat[] = rand(1, 6);
    }
    
    return $resultat;
}

if(isset($_POST['execute'])) {

    $num = isset($_POST['number']) ? intval($_POST['number']) : 0;
        
    if ($num > 0) {

        $resultat = tirarDaus($num);
        $sum = array_sum($resultat);

        echo "<h2>Rolling $num dice</h2>";

        foreach ($resultat as $imatge) {

            echo "<img src='images/$imatge.jpg' width='150' height='150'> ";
        }

        echo "<h2>Result</h2>";
        echo "<p><b>The values are:</b> ";

        foreach ($resultat as $value) {

            echo "$value ";

        }

        echo "</p>";
        echo "<p><b>Total:</b> $sum</p>";
    }
}

?>