<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    <body>
        <div class="container mt-3 text-center">
            <h1>ROLL A NUMBER OF DICE</h1>
            <form method="POST">
                <label for="numero">Number:</label>
                <br>
                <br>
                <input class ="number" type="number" id="numero" name="number" placeholder="Enter number of dices" required value="<?php echo isset($_POST['number']) ? $_POST['number'] : ''; ?>">
                <br>
                <br>
                <button class="btn btn-primary" type="submit" name="execute">ROLL</button>
            </form>
                <p>Click the button to roll a number of dice</p>
                <br>
                <?php 
                    include 'dice.php';
                ?>
        </div>
    </body>
</html>