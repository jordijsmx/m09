<?php 
    $url = "https://joanseculi.com/edt69/animes3.php";
    $data = file_get_contents($url);
    $array = json_decode($data, true);

    echo "<b>Num animes: </b>";

    $num = 0;
    foreach ($array as $object) {
        foreach ($object as $key => $value) {
            $num +=1;
        }
    }
    echo "$num";
    echo "<br>";
    echo "<br>";

    echo "<b>Genres: </b>";
    
    $array2 = array_unique($array);
    
    foreach ($array2["animes"] as $value) {
            echo " "."$value[genre],". " ";
        }
    
    echo "<br>";
    echo "<br>";

    echo "<b>Year: </b>";

    foreach ($array as $object) {
        foreach ($object as $key => $value) {
            echo " "."$value[year]" . " ";
        }
    }

    echo "<br>";
    echo "<br>";

    echo "<table>";
    foreach ($array as $object) {
        foreach ($object as $key => $value) {
            echo "<tr>";
                echo "<td rowspan=6><img src='https://joanseculi.com/$value[image]' width='250'></td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td>ID: $value[id]</td>";
                echo "<td>Type: $value[type]</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td>Name: $value[name]</td>";
                echo "<td>Year: $value[year]</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td>Original Name: $value[originalname]</td>";
                echo "<td>Rating: $value[rating]</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td>Demography: $value[demography]</td>";
                echo "<td>Genre: $value[genre]</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td colspan=2>Synopsis:<br>$value[description]</td>";
            echo "</tr>";
        }
    }
    echo "</table>";
?>