<?php
class Language
{
    // Properties

    private int $language_id;
    private string $language_code;
    private string $language_name;
    
    // Constructor

    function __construct(int $language_id, 
                        string $language_code, 
                        string $language_name)
    {

        $this->language_id = $language_id;
        $this->language_code = $language_code;
        $this->language_name = $language_name;

    }

    // Getters 

    public function getLanguageId(): int
    {
        return $this->language_id;
    }

    public function getLanguageCode(): string
    {
        return $this->language_code;
    }

    public function getLanguageName(): string
    {
        return $this->language_name;
    }
    
    // Setters

    public function setLanguageCode(string $language_code): void
    {
        $this->language_code = $language_code;
    }

    public function setLanguageName(string $language_name): void
    {
        $this->language_name = $language_name;
    }


    //toString
    public function __toString(): string
    {

        return "Language:\n" . 
        "Language_id: " . $this->get_language_id() . "\n";
        "Language_code" . $this->get_language_code() . "\n";
        "Language_name" . $this->get_language_name() . "\n";

    }
}

?>