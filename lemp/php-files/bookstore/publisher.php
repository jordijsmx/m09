<?php

class Publisher
{
    // Properties

    private int $publisher_id;
    private string $publisher_name;

    // Constructor

    function __construct(int $publisher_id, string $publisher_name)
    {
        $this->publisher_id = $publisher_id;
        $this->publisher_name = $publisher_name;
    }

    // Getters

    public function getPublisherId(): int
    {
        return $this->publisher_id;
    }

    public function getPublisherName(): string
    {
        return $this->publisher_name;
    }

    // Setters

    public function setPublisherName(string $publisher_name): void
    {
        $this->publisher_name = $publisher_name;
    }


    //toString
    public function __toString(): string
    {

        return "Publisher ID: " . $this->publisher_id . "\n" .
               "Publisher Name: " . $this->publisher_name . "\n";
    
    }
}
?>