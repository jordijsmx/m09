<?php
class Library
{
        
    // Properties

    private string $name;
    private array $books;

    // Constructor

    public function __construct(string $name)
    {

        $this->name = $name;
        $this->books = array();
    
    }
    // Getters

    public function getName(): string
    {
        return $this->name;
    }

    public function getBooks(): array
    {
        return $this->books;
    }
    
    // Setters

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setBooks(array $books): void
    {
        $this->books = $books;
    }

    // Methods

    public function add_book(Book $book): void
    {
        
        $this->books[] = $book;

    }

    public function get_books_by_title(string $title): array
    {
        $matching_books = array();

        foreach ($this->books as $book) {
            if (stripos($book->getTitle(), $title) !== false) {
                $matching_books[] = $book;
        }
    }
    return $matching_books;
    }

    public function get_books_by_isbn13(string $isbn13): array
    {
        $matching_books = array();

        foreach ($this->books as $book) {
            if ($book->getIsbn13() === $isbn13) {
                $matching_books[] = $book;
        }
    }
    return $matching_books;
    }

    public function get_books_by_author_name(string $author_name): array
    {

        foreach ($this->books as $book) {
            foreach ($book->getAuthors() as $author) {
                if (stripos($author->getAuthorName(), $author_name) !== false) {
                    $matching_books[] = $book;
                    break;
                }
            }
        }
    
        return $matching_books;
    
    }

    //toString

    public function __toString(): string
    {
        $library_info = "";
    
        foreach ($this->books as $book) {
    
            $library_info .= $book . "\n";
        }
    
        return "Library: " . $this->name . "\nBooks:\n" . $library_info;

    }
}

?>