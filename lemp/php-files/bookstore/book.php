<?php

class Book {

    // Properties
    private int $book_id;
    private string $title;
    private string $isbn13;
    private Language $language;
    private int $num_pages;
    private string $publication_date;
    private Publisher $publisher;
    private array $authors;

    // Constructor
    public function __construct(int $book_id, 
                                string $title, 
                                string $isbn13, 
                                Language $language, 
                                int $num_pages, 
                                string $publication_date, 
                                Publisher $publisher, 
                                array $authors)
    {

        $this->book_id = $book_id;
        $this->title = $title;
        $this->isbn13 = $isbn13;
        $this->language = $language;
        $this->num_pages = $num_pages;
        $this->publication_date = $publication_date;
        $this->publisher = $publisher;
        $this->authors = $authors;

    }

    // Getters

    public function getBookId(): int {
        return $this->book_id;
    }

    public function getTitle(): string {
        return $this->title;
    }

    public function getIsbn13(): string {
        return $this->isbn13;
    }

    public function getLanguage(): Language {
        return $this->language;
    }

    public function getNumPages(): int {
        return $this->num_pages;
    }

    public function getPublicationDate(): string {
        return $this->publication_date;
    }

    public function getPublisher(): Publisher {
        return $this->publisher;
    }

    public function getAuthors(): array {
        return $this->authors;
    }

    // Setters

    public function setTitle(string $title) {
        $this->title = $title;
    }

    public function setIsbn13(string $isbn13) {
        $this->isbn13 = $isbn13;
    }

    public function setLanguage(Language $language) {
        $this->language = $language;
    }

    public function setNumPages(int $num_pages) {
        $this->num_pages = $num_pages;
    }

    public function setPublicationDate(string $publication_date) {
        $this->publication_date = $publication_date;
    }

    public function setPublisher(Publisher $publisher) {
        $this->publisher = $publisher;
    }

    public function setAuthors(array $authors) {
        $this->authors = $authors;
    }

    // Methods
    public function add_author(Author $author): void 
    {

        $this->authors[] = $author;
    
    }

    public function remove_author(Author $author): void 
    {
        $index = array_search($author, $this->authors);
        
        if ($index !== false) {
            unset($this->authors[$index]);

            $this->authors = array_values($this->authors);
        }
    }


    // toString

    public function __toString(): string
    {
        return "Book:\n" . 
        "Book ID: " . $this->get_book_id() . "\n" . 
        "Title: " . $this->get_title() . "\n" . 
        "ISBN13: " . $this->get_isbn13() . "\n" .  
        "Language: " . $this->get_language() . "\n" .  
        "Num Pages: " . $this->get_num_pages() . "\n" .  
        "Publication Date: " . $this->get_publication_date() . "\n" .  
        "Publisher: " . $this->get_publisher() . "\n" .  
        "Authors: " . $this -> get_autors();
    }

}

?>