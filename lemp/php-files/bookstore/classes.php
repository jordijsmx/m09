<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="container">
        <h1>Books By Title</h1>
        <form method="GET">
            <input type="text" id="search" name="search" value="<?php echo isset($_GET['search']) ? $_GET['search'] : ''; ?>">
            <button class='btn btn-primary' type="submit">Search</button>
        </form>
        <br>

        <?php
        include_once('book.php');
        include_once('language.php');
        include_once('publisher.php');
        include_once('author.php');

        $servername = "172.20.0.2";
        $username = "root";
        $password = "1234";
        $dbname = "bookstore";

        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        if (isset($_GET['search'])) {
            $search = '%' . $_GET['search'] . '%';

            $sql = "SELECT *
                    FROM book b
                    INNER JOIN book_language bl ON b.language_id = bl.language_id
                    INNER JOIN publisher p ON b.publisher_id = p.publisher_id
                    INNER JOIN book_author ba ON b.book_id = ba.book_id
                    INNER JOIN author a ON a.author_id = ba.author_id
                    WHERE LOWER(title) LIKE ?
                    ORDER BY b.book_id";

            $stmt = $conn->prepare($sql);
            $stmt->bind_param("s", $search);
            $stmt->execute();
            $result = $stmt->get_result();

            if ($result->num_rows > 0) {
                echo "<table class='table table-striped'>";
                echo "<tr><th>ID</th><th>TITLE</th><th>ISBN13</th><th>NUM_PAGES</th><th>PUBL. DATE</th><th>PUBLISHER</th><th>LANGUAGE</th><th>AUTHOR</th></tr>";

                $books = array();

                while ($row = $result->fetch_assoc()) {
                    $language = new Language($row['language_id'], $row['language_code'], $row['language_name']);
                    $publisher = new Publisher($row['publisher_id'], $row['publisher_name']);
                    $author = new Author($row['author_id'], $row['author_name']);

                    if (array_key_exists($row['book_id'], $books)) {
                        $books[$row['book_id']]['authors'][] = $author;
                    } else {
                        $books[$row['book_id']] = array(
                            'book' => new Book(
                                $row['book_id'],
                                $row['title'],
                                $row['isbn13'],
                                $language,
                                $row['num_pages'],
                                $row['publication_date'],
                                $publisher,
                                array($author)
                            ),
                            'authors' => array($author)
                        );
                    }
                }

                foreach ($books as $bookInfo) {
                    $book = $bookInfo['book'];
                    $authors = $bookInfo['authors'];

                    echo "<tr>";
                    echo "<td>" . $book->getBookId() . "</td>";
                    echo "<td>" . $book->getTitle() . "</td>";
                    echo "<td>" . $book->getIsbn13() . "</td>";
                    echo "<td>" . $book->getNumPages() . "</td>";
                    echo "<td>" . $book->getPublicationDate() . "</td>";
                    echo "<td>" . $book->getPublisher()->getPublisherName() . "</td>";
                    echo "<td>" . $book->getLanguage()->getLanguageName() . "</td>";
                    echo "<td>";
                    foreach ($authors as $key => $author) {
                        echo $author->getAuthorName();
                        if ($key < count($authors) - 1) {
                            echo ", ";
                        }
                    }
                    echo "</td>";
                    echo "</tr>";
                }

                echo "</table>";
            } else {
                echo "No matching results found.";
            }

            $stmt->close(); 
        }

        $conn->close(); 
        ?>

    </div>
</body>
</html>