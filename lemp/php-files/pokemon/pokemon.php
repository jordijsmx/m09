<?php

// Properties
class Pokemon {
    private $code;
    private $name;
    private $type1;
    private $type2;
    private $healthPoints;
    private $attack;
    private $defense;
    private $specialAttack;
    private $specialDefense;
    private $speed;
    private $generation;
    private $legendary;
    private $image;
    private $total;

// Constructor 

    public function __construct($code, $name, $type1, $type2, $healthPoints, $attack, $defense, $specialAttack, $specialDefense, $speed, $generation, $legendary, $image) {
            $this->code = $code;
            $this->name = $name;
            $this->type1 = $type1;
            $this->type2 = $type2;
            $this->healthPoints = $healthPoints;
            $this->attack = $attack;
            $this->defense = $defense;
            $this->specialAttack = $specialAttack;
            $this->specialDefense = $specialDefense;
            $this->speed = $speed;
            $this->generation = $generation;
            $this->legendary = $legendary;
            $this->image = $image;
            $this->total = $this->total();
        }

// Getters

public function getCode(): int {
    return $this->code;
}

public function getName(): string {
    return $this->name;
}

public function getType1(): string {
    return $this->type1;
}

public function getType2(): string {
    return $this->type2;
}

public function getHealthPoints():int {
    return $this->healthPoints;
}

public function getAttack():int {
    return $this->attack;
}

public function getDefense():int {
    return $this->defense;
}

public function getSpecialAttack():int {
    return $this->specialAttack;
}

public function getSpecialDefense():int {
    return $this->specialDefense;
}

public function getSpeed():int {
    return $this->speed;
}

public function getGeneration():int {
    return $this->generation;
}

public function getLegendary():bool {
    return $this->legendary;
}

public function getImage():string {
    return $this->image;
}

public function getTotal():int {
    return $this->total;
}

// Setters

public function setCode($code) {
    $this->code = $code;
}

public function setName($name) {
    $this->name = $name;
}

public function setType1($type1) {
    $this->type1 = $type1;
}

public function setType2($type2) {
    $this->type2 = $type2;
}

public function setHealthPoints($healthPoints) {
    $this->healthPoints = $healthPoints;
}

public function setAttack($attack) {
    $this->attack = $attack;
}

public function setDefense($defense) {
    $this->defense = $defense;
}

public function setSpecialAttack($specialAttack) {
    $this->specialAttack = $specialAttack;
}

public function setSpecialDefense($specialDefense) {
    $this->specialDefense = $specialDefense;
}

public function setSpeed($speed) {
    $this->speed = $speed;
}

public function setGeneration($generation) {
    $this->generation = $generation;
}

public function setLegendary($legendary) {
    $this->legendary = $legendary;
}

public function setImage($image) {
    $this->image = $image;
}

//toString Method

public function __toString() {
    return "Pokemon:\n" .
           "Code: " . $this->getCode() .
           "Name: " . $this->getName() .
           "Type 1: " . $this->getType1() .
           "Type 2: " . $this->getType2() .
           "Health Points: " . $this->getHealthPoints() .
           "Attack: " . $this->getAttack() .
           "Defense: " . $this->getDefense() .
           "Special Attack: " . $this->getSpecialAttack() .
           "Special Defense: " . $this->getSpecialDefense() .
           "Speed: " . $this->getSpeed() .
           "Generation: " . $this->getGeneration() .
           "Legendary: " . $this->getLegendary() .
           "Image: " . $this->getImage() .
           "Total: " . $this->total();
}

// Total Method
    public function total() : int {
        return $this->healthPoints + 
               $this->attack + 
               $this->defense + 
               $this->specialAttack + 
               $this->specialDefense + 
               $this->speed;
    }
}
?>