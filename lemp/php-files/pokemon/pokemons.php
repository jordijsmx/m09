<?php


class Pokemons {

    // Properties
    private $pokemons;

    // Constructor
    public function __construct() {
        $this->pokemons = array();
    }

    // Getters
    public function getPokemons(): array
    {
        return $this->pokemons;
    }

    // Setters
    public function setPokemons($pokemons) {
        $this->pokemons = $pokemons;
    }

    // Add Method
    public function add_pokemon(Pokemon $pokemon) {
        $this->pokemons[] = $pokemon;
    }

    // Generation Method
    public function get_generation(int $generation) {

        $generation_pokemons = array();

        foreach ($this->pokemons as $pokemon) {
            if ($pokemon->getGeneration() === $generation) {
                $generation_pokemons[] = $pokemon;
            }
        }
        return $generation_pokemons;
    }

    // Pokemon Name Method
    public function get_pokemon_name(string $text) {
        
        $text = strtolower($text);
        $matching_pokemons = array();
        
        foreach ($this->pokemons as $pokemon) {
            if (strpos(strtolower($pokemon->getName()), $text) !== false) {
                $matching_pokemons[] = $pokemon;
            }
        }
        return $matching_pokemons;
    }

    // Pokemon Type Method
    public function get_pokemon_type(string $text) {

        $text = strtolower($text); 
        $matching_pokemons = array();

        foreach ($this->pokemons as $pokemon) {
            if (strpos(strtolower($pokemon->getType1()), $text) !== false || strpos(strtolower($pokemon->getType2()), $text) !== false) {
                $matching_pokemons[] = $pokemon;
            }
        }
        return $matching_pokemons;
    }
}
?>