<?php

require_once 'pokemon.php';
require_once 'pokemons.php';


$pokemons = new Pokemons();

$url = "https://joanseculi.com/json/pokemons.json";

$json_data = file_get_contents($url);
$data = json_decode($json_data, true);


foreach ($data['pokemons'] as $pokemon_data) {

    $pokemon = new Pokemon(
        $pokemon_data['Code'],
        $pokemon_data['Name'],
        $pokemon_data['Type1'],
        $pokemon_data['Type2'],
        $pokemon_data['HealthPoints'],
        $pokemon_data['Attack'],
        $pokemon_data['Defense'],
        $pokemon_data['SpecialAttack'],
        $pokemon_data['SpecialDefense'],
        $pokemon_data['Speed'],
        $pokemon_data['Generation'],
        $pokemon_data['Legendary'],
        $pokemon_data['Image']
    );

    $pokemons->add_pokemon($pokemon);
}


echo "Generation: ";
echo "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";
  echo "<div class='row'>";
    echo "<div class='col-md-6'>";
      echo "<select name='generacio' class='form-control'>";
        echo "<option value='All'>All</option>";
        for ($i = 1; $i <= 6; $i++) {
            $selected = isset($_POST['generacio']) && $_POST['generacio'] == $i ? 'selected' : '';
            echo "<option value='$i' $selected>$i</option>";
        }

      echo "</select>";
    echo "</div>";
    echo "<div class='col-md-6'>";
      echo "<button class='btn btn-primary' type='submit' name='execute'>Search</button>";
    echo "</div>";
  echo "</div>";


echo "Name: ";
echo "<div class='row'>";
  echo "<div class='col-md-6'>";
        echo "<input class='form-control' name='pokemon_name' placeholder='Enter a Pokemon name...' value='" . (isset($_POST['pokemon_name']) ? htmlspecialchars($_POST['pokemon_name']) : '') . "'>";
  echo "</div>";
    echo "<div class='col-md-6'>";
      echo "<button class='btn btn-primary' type='submit' name='execute_name'>Search</button>";
    echo "</div>";
  echo "</div>";

  echo "Type: ";
  echo "<div class='row'>";
    echo "<div class='col-md-6'>";
        echo "<input class='form-control' name='pokemon_type' placeholder='Enter a Pokemon type...' value='" . (isset($_POST['pokemon_type']) ? htmlspecialchars($_POST['pokemon_type']) : '') . "'>";
    echo "</div>";
    echo "<div class='col-md-6'>";
      echo "<button class='btn btn-primary' type='submit' name='execute_type'>Search</button>";
    echo "</div>";
  echo "</div>";

  echo "</form>";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['execute'])) {
        generacio($pokemons);
    } elseif (isset($_POST['execute_name'])) {
        nom($pokemons);
    } elseif (isset($_POST['execute_type'])) {
        tipus($pokemons);
    }
}

function generacio($pokemons) {
    $selected_generation = $_POST["generacio"];

    if ($selected_generation == "All") {
        $filtered_pokemons = $pokemons->getPokemons();
    } else {
        $filtered_pokemons = $pokemons->get_generation($selected_generation);
    }

    taula($filtered_pokemons);
}

function nom($pokemons) {
    $nom = $_POST["pokemon_name"];
    $filtered_pokemons = $pokemons->get_pokemon_name($nom);
    taula($filtered_pokemons);
}

function tipus($pokemons) {
    $tipus = $_POST["pokemon_type"];
    $filtered_pokemons = $pokemons->get_pokemon_type($tipus);
    taula($filtered_pokemons);
}

function taula($filtered_pokemons) {
  
    echo "<table class='table table-striped'>";
    echo "<thead class='table-dark' style='text-align:center'>";
      echo "<tr>";
        echo "<th>Image</th>";
        echo "<th>Code</th>";
        echo "<th>Name</th>";
        echo "<th>Type 1</th>";
        echo "<th>Type 2</th>";
        echo "<th>HP</th>";
        echo "<th>ATK</th>";
        echo "<th>DEF</th>";
        echo "<th>SP_ATK</th>";
        echo "<th>SP_DEF</th>";
        echo "<th>SPD</th>";
        echo "<th>GEN</th>";
        echo "<th>LEG</th>";
        echo "<th>TOTAL</th>";
      echo "</tr>";
    echo "</thead>";
    echo "<tbody style='text-align:center'>";

    foreach ($filtered_pokemons as $pokemon) {
        echo "<tr>";
        echo "<td><img src='{$pokemon->getImage()}' alt='{$pokemon->getName()}' width='200'></td>";
        echo "<td>{$pokemon->getCode()}</td>";
        echo "<td>{$pokemon->getName()}</td>";
        echo "<td>{$pokemon->getType1()}</td>";
        echo "<td>{$pokemon->getType2()}</td>";
        echo "<td>{$pokemon->getHealthPoints()}</td>";
        echo "<td>{$pokemon->getAttack()}</td>";
        echo "<td>{$pokemon->getDefense()}</td>";
        echo "<td>{$pokemon->getSpecialAttack()}</td>";
        echo "<td>{$pokemon->getSpecialDefense()}</td>";
        echo "<td>{$pokemon->getSpeed()}</td>";
        echo "<td>{$pokemon->getGeneration()}</td>";
        echo "<td>" . ($pokemon->getLegendary() ? 'Yes' : 'No') . "</td>";
        echo "<td>{$pokemon->getTotal()}</td>";
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
}

?>