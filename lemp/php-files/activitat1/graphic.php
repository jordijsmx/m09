<?php $games = array(
                    
                    "Nintendo DS" => 154,
                    "Game Boy" => 119,
                    "Nintendo Switch" => 60,
                    "Playstation 2" => 1555,
                    "Wii" => 101,
                    "Playstation 3" => 87,
                    "Xbox 360" => 84,
                    "Playstation Portable" => 82,
                    "Playstation 4" => 102,
                    "Game Boy Advance" => 81,
                    "Nintendo 3DS" => 72,
                    "Nes" => 62,
                  
                );
                
        arsort($games);

        $valor_max = max($games);

        $factor = 100 / $valor_max;

        foreach ($games as $key => $value) {
            echo "<tr>";
            echo "<td>" . $key . ": </td>";
            echo "<td>";

            $valor = ($value * $factor);

            for ($i = 0; $i < $valor; $i++) {
                echo "<img src='green.png'/>";
            }
            echo " ".$value." Millions";
            echo "</td>";
            echo "</tr>";
        }
?>