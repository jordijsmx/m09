<?php
    class Card
        {
        // Properties
        private int $value;
        private int $suit;
        private string $image;
        // Constructor
        public function __construct(    
            int $value,
            int $suit,
            string $image)

            {
                $this->value = $value;
                $this->suit = $suit;
                $this->image = $image;
            }

        // Getters
        public function getValue(): int
        {
            return $this->value;
        }

        public function getSuit(): int
        {
            return $this->suit;
        }

        public function getImage(): string
        {
            return $this->image;
        }

        // Setters
        public function setValue(int $value): void
        {
            $this->value = $value;
        }

        public function setSuit(int $suit): void
        {
            $this->suit = $suit;
        }

        public function setImage(string $image): void
        {
            $this->image = $image;
        }

        // toString

        public function __toString(): string
        {
            return $this->getValue() . 
                   $this->getSuit() . 
                   $this->getImage();
        }
        
    }
?>