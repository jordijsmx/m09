<?php
    require_once 'card.php';
    class Hand
    {
        // Properties
        private array $cards;
        // Constructor
        public function __construct()
        {
            $this->cards = array();
        }

        // Getters
        public function getCards(): array
        {
            return $this->cards;
        }
        
        // Setters
        public function setCards($cards): void
        {
            $this->cards = $cards;
        }

        
        // Methods
        public function addCard(Card $card): void
        {
            $this->cards[] = $card;
        }
        
        //toString
        public function __toString()
        {
           return "Cards" . $this->getCards();
        }
    }
?>