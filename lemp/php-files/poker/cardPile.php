<?php

    require_once 'card.php';
class CardPile{
    private array $pile;

    // Constructor
    public function __construct()
    {
        $this->pile = array();
    }

    // Methods
    // Function to add a card to the pile
    public function addCard(Card $card) 
    {
        $this->pile[] = $card;
    }

    // Function to take a card from the pile
    public function removeCard(Card $card): Card 
    {
        $index = array_search($card, $this->pile, true);
        if ($index !== false) {
            $removedCard = $this->pile[$index];
            array_splice($this->pile, $index, 1);
            return $removedCard;
        }

        return null;
    }
    
    // Getters
    public function getPile() : array
    {
        return $this->pile;
    }

    // Setters
    public function setPile(array $pile)
    {
        $this->pile = $pile;
    }
}
?>