<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    
    <body>
        <div class="container">
            <h1>POKER HAND</h1>
            <form method="POST">
                <button class="btn btn-primary" type="submit" name="execute">GIVE CARDS</button>
            </form>
            <p>Click the button to get your cards.</p>
            <?php 
                include 'app-alumne.php';
            ?>
        </div>
    </body>
</html>