<?php
include_once("card.php");
include_once("hand.php");
include_once("cardPile.php");
$hand = new Hand();

if ($_POST) {
  if (isset($_POST['execute'])) {
    $cardPile = generateCards();
    $hand = getCards($cardPile);
    showCards($hand);
    $text = getPoints($hand);
    showPoints($text);
    $total = getTotal($hand);
    showTotal($total);
    evaluate($hand);
  }
}

function generateCards(): CardPile
{
  $cardPile = new CardPile();

  for ($i=1; $i <= 13; $i++) {
    for ($j=1; $j <= 4 ; $j++) { 
        $card = new Card($i, $j, "images/card" . ($i + ($j - 1) * 13 ) . ".gif");
        $cardPile->addCard($card);
    } 
  }

  return $cardPile;
}

function getCards(CardPile &$cardPile): Hand
{
    $hand = new Hand();

    for ($i = 0; $i < 5; $i++) {
      $randomIndex = array_rand($cardPile->getPile());
      $randomCard = $cardPile->getPile()[$randomIndex];
      $hand->addCard($randomCard);
      $cardPile->removeCard($randomCard);
    }

    return $hand;
}

function showCards(Hand &$hand)
{
  echo "<h3>Poker hand</h3>";

    foreach ($hand->getCards() as $card) {
        echo '<img src="' . $card->getImage() .'" alt="Card" />';
    }
}

function getPoints(&$hand): string
{
  $points = '';
  foreach ($hand->getCards() as $card) {

      $value = $card->getValue();

      if ($value == 1){
        $value = 14;
      }

      $points .= $value . ' ';
  }

  $points = rtrim($points);

  return $points;
}

function showPoints($text)
{
  echo "<br></br>";
  echo "<h2>Result</h2>";
  echo "<p>CARD POINTS: $text</p>";
}

function getTotal(&$hand): int
{
  $total = 0;
  foreach ($hand->getCards() as $card) {
      $value = $card->getValue();

      if ($value == 1) {
          $value = 14;
      }

      $total += $value;
  }

  return $total;
}

function showTotal($total)
{
  echo "<p>TOTAL POINTS: $total</p>";
}

function evaluate(&$hand)
{

}

function isRoyalFlush($hand): bool
{

}


function isStraightFlush($hand): bool
{

}

function isRepoker($hand): bool
{

}

function isPoker($hand): bool
{
  
}

function isFullHouse($hand): bool
{
    //TODO
    return false;
}

function isFlush($hand): bool
{
    //TODO
    return false;
}

function isStraight($hand): bool
{
    //TODO
    return false;

}


function isThreeOfAKind($hand): bool
{
    //TODO
    return false;
}

function isTwoPairs($hand): bool
{
    //TODO
    return false;
}

function isOnePair($hand): bool
{
    //TODO
    return false;
}
?>