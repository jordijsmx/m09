<?php
$h = date("H");
$y = date("Y");
$d = date("Y/m/d");
echo $h . "<br>";
echo $y . "<br>";
echo $d . "<br>";

if ($h < "10") {
    echo "Have a good morning!";
} elseif ($h < "20") {
    echo "Have a good day!";
} else {
    echo "Have a good night!";
}
?>