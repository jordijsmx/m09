<?php
$servername = "172.19.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: ". $conn->connect_error);
} else {
    echo "Successfully connected" . "<br>";
}

// SELECTS

$sql = "UPDATE book SET title = ? WHERE book_id = ?";

$stmt = $conn->prepare($sql);
$stmt->bind_param("issiisi", $id, $title,$isbn,$language_id,$num_pages,$publication_date,$publisher_id);

$id = 11129;
$title = "New Book";
$isbn = "111111";
$language_id = 1;
$num_pages = 100;
$publication_date = "2023-05-03";
$publisher_id = 1;

$stmt->execute();
$result = $stmt->get_result(); 

if ($stmt->affected_rows > 0) {
    // output data of each row
    echo "Num rows inserted: " . $stmt->affected_rows;

} else {
    echo "0 results";
}


$conn->close();
?>