<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="container">
    <h1>Books By ID</h1>
    <form method="GET">
        <input type="text" id="search" name="search">
        <button class="btn btn-primary" type="submit">Search</button>
    </form>
    <br>
    <?php
    $servername = "172.19.0.2";
    $username = "root";
    $password = "1234";
    $dbname = "bookstore";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);

    }

    if (isset($_GET['search'])) {
        $search = $_GET['search'];

        $sql = "SELECT * FROM book WHERE book_id LIKE '%$search%' ORDER BY book_id";

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            echo "<table class='table table-stripped'>";
            echo "<tr><th>ID</th><th>TITLE</th><th>ISBN13</th><th>PUBL. DATE</th></tr>";

            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["book_id"] . "</td>";
                echo "<td>" . $row["title"] . "</td>";
                echo "<td>" . $row["isbn13"] . "</td>";
                echo "<td>" . $row["publication_date"] . "</td>";
                echo "</tr>";
            }
            echo "</table>";
        } else {
            echo "No matching results found.";
        }
    }

    $conn->close();
    ?>
    </div>
</body>
</html>