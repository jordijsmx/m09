<?php

$url = "https://www.thesportsdb.com/api/v1/json/3/all_countries.php";

$json_data = file_get_contents($url);
$data = json_decode($json_data, true);

$eleccio = isset($_POST['country']) ? $_POST['country'] : '';

echo "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";
  echo "<div class='row'>";
    echo "<div class='col-md-6'>";
      echo "<select name='country' class='form-control'>";

          foreach ($data['countries'] as $country) {
            $pais = $country['name_en'];
            $selected = ($eleccio == $pais) ? 'selected' : '';
            echo "<option value='$pais' $selected>$pais</option>";
          }

      echo "</select>";
    echo "</div>";

    echo "<div class='col-md-6'>";
      echo "<button class='btn btn-primary' type='submit' name='execute'>Search</button>";
    echo "</div>";

  echo "</div>";
echo "</form>";

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['execute'])) {
  execute($_POST['country']);
}

function execute($country) {
  
  $url = "https://www.thesportsdb.com/api/v1/json/3/search_all_leagues.php?c=" . $country; 

  $json_data = file_get_contents($url);
  $data = json_decode($json_data, true);
 
  echo "<table class='table table-striped'>";
    echo "<thead class='table-dark' style='text-align:center'>";
      echo "<tr>";
        echo "<th>Flag</th>";
        echo "<th>Title</th>";
        echo "<th>Website</th>";
        echo "<th>Image</th>";
      echo "</tr>";
    echo "</thead>";
    echo "<tbody style='text-align:center'>";

    if (isset($data['countries'])) {
      foreach ($data['countries'] as $league) {
        echo "<tr>";
          echo "<td><img src='{$league['strBadge']}' alt='{$country}' width='100'></td>";
          echo "<td>{$league['strLeague']}</td>";
          echo "<td>";

            if (!empty($league['strWebsite'])) {
              echo "<a href=https://{$league['strWebsite']}'>https://{$league['strWebsite']}</a>";
            } else {
              echo "No Website Found";
            }
          
          echo "</td>";
          echo "<td>";
  
              if (!empty($league['strFanart1'])) {
                echo "<a href='{$league['strFanart1']}' target='_blank'><img src='{$league['strFanart1']}' alt='{$league['strLeague']}' width='300' class='rounded'></a>";
              } else {
                echo "No Image Found";
              }
         
          echo "</td>";
          echo "</tr>";
          echo "<tr>";
                echo "<td colspan='4' style='text-align: justify;'>{$league['strDescriptionEN']}</td>";
            echo "</tr>";
      }
    }
    echo "</tbody>";
  echo "</table>";
}
