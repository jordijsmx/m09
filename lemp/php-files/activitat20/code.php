<?php

$json_file = 'data.json';
$json_data = file_get_contents($json_file);

$data = json_decode($json_data, true);

foreach ($data as $mountain) {

    echo "<tr>";
    echo "<td><img src='" . $mountain['url'] . "' width='300'></td>";
    echo "<td>";
    
    echo "Name: " . $mountain['name'] . "<br>";
    echo "Height: " . $mountain['height'] . " m<br>";
    echo "Prominence: " . $mountain['prominence'] . " m<br>";
    echo "Zone: " . $mountain['zone'] . "<br>";
    echo "Country: " . $mountain['country'] . "<br>";

    echo "</td>";
    echo "</tr>";
}
?>