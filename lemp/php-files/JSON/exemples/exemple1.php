<?php
$age = array("Peter" => 35, "Ben" => 37, "Joe" => 43);

$json = json_encode($age);

var_dump($json);

echo "<br>";

$jsonobj = '{"Peter":35,"Ben":37,"Joe":43}';

$array2 = json_decode($jsonobj, true); // si posem true és array associatiu? clau > valor

var_dump($array2);

echo "<br>";

echo $array2->Peter . "<br>";
echo $array2->Ben. "<br>";
echo $array2->Joe . "<br>";


$array3 = json_decode($jsonobj, true); 

var_dump($array3);

echo "<br>";
echo $array3["Peter"] . "<br>";
echo $array3["Ben"] . "<br>";
echo $array3["Joe"] . "<br>";
echo "<br>";

$url = "https://jsonplaceholder.typicode.com/posts/1"; // path to your JSON file
$data = file_get_contents($url); // put the contents of the file into a variable
$obj = json_decode($data); // decode the JSON feed

//Iterating $obj
foreach ($obj as $key => $value) {
    echo "[" . $key . ", " . $value . "]" . "<br>";
}

//JSON Array
  //$jsonArray = [{"name": "John", "age": 25}, {"name": "Jane", "age": 30}] a fitxer data2.json

  
?>