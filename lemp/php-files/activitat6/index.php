<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="body">
            <h2>Factorial Calculator: n!</h2>
            <div class="calcul">
            <p>Enter a number to calculate de factorial n! using the calculator below.</p>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <label for="numero">Number:</label>
                <input type="number" id="numero" name="numero" placeholder="Enter a number" value="<?php echo isset($_POST['numero']) ? $_POST['numero'] : '';; ?>">
                <br>
                <br>
                <button type="submit">SEND</button>
            </form>
            </div>
            <p>Factorial Number:</p>
            <p>n! = <?php include "factorial.php"; ?></p>
            </div>
            <br>
            <h5>Factorial Formulas:</h5>
            <p>The formula to calculate a factorial for a number is:</p>
            <p><em>n! = n × (n-1) × (n-2) × … × 1</em></p>
            <p>Thus, the factorial n! is equal to n times n minus 1, times n minus 2, continuing until the number 1 is reached.</p>
            <p>The factorial of zero is 1:</p>
            <p><em>0! = 1</em></p>
            <h5>Recurrence Relation:</h5>
            <p>And the formula expressed using the recurrence relation looks like this:</p>
            <p><em>n! = n × (n – 1)!</em></p>
            <p>So the factorial n! is equal to the number n times the factorial of n minus one. This recurses until n is equal to 0.</p>
            <br>
            <h5>Factorial Table</h5>
            <p>The table below shows the factorial n! for the numbers one to one-hundred.</p>

            <table>
                <tr>
                    <th>n</th>
                    <th>n!</th>
                </tr>
                <?php 
                    include 'taula.php';
                ?>

            </table>
        </div>
    </body>
</html>