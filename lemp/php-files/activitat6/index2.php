<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    <body>
    <div class="fondo">
        <div class="fondo2">
            <h2>Factorial Calculator: n!</h2>
            <div class="factorial">     
                <div class="calcul">
                <?php include "factorial2.php";?>
                <p>Enter a number to calculate de factorial n! using the calculator below.</p>
                <form method="POST">
                    <label for="numero">Number:</label>
                    <input class ="number" type="number" id="numero" name="number" placeholder="Enter a number" value="<?php echo $number; ?>">
                    <br>
                    <br>
                    <button class="boto" type="submit" name="execute">SEND</button>
                </form>
                </div>
                <div class="gris">
                    <p>Factorial Number:</p>
                    <p>n! = <?php echo execute(); ?></p>
                </div>
            </div>

            <br>
            <h5>Factorial Formulas:</h5>
            <p>The formula to calculate a factorial for a number is:</p>
            <p><em>n! = n × (n-1) × (n-2) × … × 1</em></p>
            <p>Thus, the factorial n! is equal to n times n minus 1, times n minus 2, continuing until the number 1 is reached.</p>
            <p>The factorial of zero is 1:</p>
            <p><em>0! = 1</em></p>
            <h5>Recurrence Relation:</h5>
            <p>And the formula expressed using the recurrence relation looks like this:</p>
            <p><em>n! = n × (n – 1)!</em></p>
            <p>So the factorial n! is equal to the number n times the factorial of n minus one. This recurses until n is equal to 0.</p>
            <br>
            <div>
                <h5>Factorial Table</h5>
                <p>The table below shows the factorial n! for the numbers one to one-hundred.</p>
                <div class="taula"><?php echo factorialTable(); ?></div>
            </div>
        </div>
    </div>
    </body>
</html>