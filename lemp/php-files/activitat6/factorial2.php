<?php

$number = "";

if ($_POST) {
    if (isset($_POST['execute'])) {
        $number = isset($_POST['number']) ? validate($_POST['number']) : ""; 
        $factorial = factorial($number);
    }
}



function execute()
{
    $fac = "";
    $number = "";

    if (isset($_POST['execute'])) {

        if (isset($_POST['number']) && $_POST['number'] != "") {
            $number = validate($_POST['number']);

            $fac = factorial($number);
        }

        return $fac;
    }
}
function validate($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function factorial($val)
{

    if ($val == 0) return 1;
    $factorial = 1;
    for ($i = $val; $i > 1; $i--) {
        $factorial *= $i;
    }
    return $factorial;

}

function factorialTable()
{
    echo "<table>";
    echo "<tr>";
    echo "<th>n</th>";
    echo "<th>n!</th>";
    echo "</tr>";

    for ($i = 1; $i <= 100; $i++) {
        $factorial = 1;
    for ($j = 1; $j <= $i; $j++) {
        $factorial *= $j;
    }

    echo "<tr>";
    echo "<td>$i</td>";
    echo "<td>$factorial</td>";
    echo "</tr>";

    }

    echo "</table>";
}
?>