<?php 
declare(strict_types=1);

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

final class Multiples5and7Test extends TestCase {
    
    //1 
    #[Test]
    #[TestDox("Test Multiples of 5 or 7")]
    public function testMultiples5and7(): void {

        require_once __DIR__ . "/../../src/php/Multiples5and7.php";

        $mult = new Multiples5and7();


        $this->assertSame(22, $mult->multiples5and7(10));
        $this->assertSame(22, $mult->multiples5and7(12));
        $this->assertSame(51, $mult->multiples5and7(15));
        $this->assertSame(1680, $mult->multiples5and7(100));
        $this->assertSame(5, $mult->multiples5and7(5));
        $this->assertSame(5, $mult->multiples5and7(6)); 
        $this->assertSame(12, $mult->multiples5and7(9));
        $this->assertSame(0, $mult->multiples5and7(4));
        $this->assertSame(0, $mult->multiples5and7(2));
        $this->assertSame(0, $mult->multiples5and7(0));
        $this->assertSame(0, $mult->multiples5and7(-1));

    }
}
    
