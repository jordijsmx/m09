<?php 
declare(strict_types=1);

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

final class LowAndHighTest extends TestCase {

    #[Test]
    #[TestDox("Low and High")]
    public function testLowAndHigh() {

        require_once __DIR__ . "/../../src/php/LowAndHigh.php";

        $lh = new LowAndHigh();

        $this->assertSame([-9, 42], $lh->lowAndHigh("8 3 -5 42 -1 0 0 -9 4 7 4 -4"));
        $this->assertSame([1, 3], $lh->lowAndHigh("1 2 3"));
        $this->assertSame([1, 1], $lh->lowAndHigh("1 1 1"));
        $this->assertSame([], $lh->lowAndHigh(""));

    }

}
    
