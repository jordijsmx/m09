<?php 
declare(strict_types=1);

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

final class BinaryTest extends TestCase {

   
//8. Binary
#[Test]
#[TestDox("Binary")]
public function testBinary() {

    require_once __DIR__ . "/../../src/php/Binary.php";

    $binary = new Binary();

    $this->assertSame(1, $binary->binary2Number([0,0,0,1]));
    $this->assertSame(2, $binary->binary2Number([0,0,1,0]));
    $this->assertSame(15, $binary->binary2Number([1,1,1,1]));
    $this->assertSame(103, $binary->binary2Number([0,1,1,0,0,1,1,1]));
    $this->assertSame(255, $binary->binary2Number([1,1,1,1,1,1,1,1]));
    $this->assertSame(128, $binary->binary2Number([1,0,0,0,0,0,0,0]));
    $this->assertSame(-1, $binary->binary2Number([]));


  }


}
    
