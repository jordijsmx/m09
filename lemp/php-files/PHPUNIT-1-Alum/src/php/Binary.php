<?php
class Binary
{

  /*
8. Binary

Donat un array ($arr) que conté el nombre en binari, retornar el número en decimal.
Si $arr està buida, el mètode retorna un -1.

Exemples:


binary2Number([0,0,1,0]); 		// return 2
binary2Number([0,1,1,0,0,1,1,1]); 	// return 15
binary2Number([1,1,1,1,1,1,1,1]); 	// return 256
binary2Number([1,0,0,0,0,0,0,0]); 	// return 128
binary2Number([]); 			// return -1

Executar proves:
Obrir terminal.
Siturar-se al directori "tests".
Executar el test unitari: 
php phpunit.phar .\unit\BinaryTest.php


  */

  public function binary2Number(array $arr): int
  {
    if (empty($arr)) {
      return -1;
    }
  
    $decimal = 0;
    $power = 1;
  
  
    for ($i = count($arr) - 1; $i >= 0; $i--) {
  
      if ($arr[$i] == 1) {
          $decimal += $power;
      }
     
      $power *= 2;
    }
  
  return $decimal;
  }
}
