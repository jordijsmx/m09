<?php
class DetectVowels
{

  /*
  4. Detect vowels

Detecte si un string conté totes les vocals (no importa si són majúscules o minúscules).

Tots els strings es consideren sense accents.

Exemples:

detectVowels("OUbacity e hhy"); 	// retrun true
detectVowels("e412e 6obnh i jk u"); 	// return false

Executar proves:
Obrir terminal.
Siturar-se al directori "tests".
Executar el test unitari: 
php phpunit.phar .\unit\DetectVowelsTest.php

*/

  public function detectVowels(string $string): bool
  {
    $string = strtolower($string);
    
    $vowels = ['a', 'e', 'i', 'o', 'u'];
    
    foreach ($vowels as $vowel) {
        
        if (strpos($string, $vowel) === false) {
            return false;
        }
    }
    
    return true;
  }
}
