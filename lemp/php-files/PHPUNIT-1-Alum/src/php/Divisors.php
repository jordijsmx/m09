<?php
class Divisors
{

  /*
6. Divisors

Aquesta funció pren com a paràmetre un nombre > 1 ($integer) i retorna un array amb tots els enters divisors de $integer (excepte per 1 i el mateix nombre $integer).
Mostra els valors de més gran a més petit.

Si el nombre és un nombre primer (prime) retorna un string '(integer) is prime'.

Examples:

divisors(12); 	// return [6, 4, 3, 2]
divisors(100); 	// return [50, 25, 20, 10, 5, 4, 2]
divisors(13); // => '13 is prime'

Executar proves:
Obrir terminal.
Siturar-se al directori "tests".
Executar el test unitari: 
php phpunit.phar .\unit\DivisorsTest.php


*/

  public function divisors(int $integer): array | string
  {
    if ($integer <= 1) {
      return "Number must be greater than 1";
    }
  
    $divisors = [];
    for ($i = $integer - 1; $i > 1; $i--) {
      if ($integer % $i === 0) {
          $divisors[] = $i;
      }
    }
  
    if (empty($divisors)) {
      return "$integer is prime";
    }
  
  return $divisors;
  }
}
